import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:klimatic/util/utils.dart' as util;

class Klimatic extends StatefulWidget {
  @override
  _KlimaticState createState() => _KlimaticState();
}

class _KlimaticState extends State<Klimatic> {

  String _cityEntered;

  Future _goToNextScreen(BuildContext context) async {
    Map results = await Navigator.of(context).push(
      MaterialPageRoute<Map>(
        builder: (BuildContext context) {
          return ChangeCity();
        }
      )
    );
    if(results != null && results.containsKey('enter')) {
      _cityEntered = results['enter'];
    }
  }

  void showStuff() async {
    Map data = await getWeather(util.appId, util.defaultCity);
    print(data.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Klimatic',
        ),
        backgroundColor: Colors.red,
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.menu
              ),
              onPressed: () {
                _goToNextScreen(context);
              }
          ),
        ],
      ),
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage('images/umbrella.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget>[
            Container(
              alignment: Alignment.topRight,
              margin: EdgeInsets.fromLTRB(0.0, 10.9, 20.9, 0.0),
              child: Text(
                '${_cityEntered == null ? util.defaultCity : _cityEntered}',
                style: cityStyle(),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: Image.asset('images/light_rain.png'),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20.0, 400.0, 0.0, 0.0),
              child: updateTempWidget(_cityEntered)
            )
          ],
        ),
      ),
    );
  }

  Future<Map> getWeather(String appId, String city) async {
    String apiUrl = 'http://api.openweathermap.org/data/2.5/weather?q=$city'
        '&appid=${util.appId}&units=metric';
    http.Response response = await http.get(apiUrl);
    return json.decode(response.body);
  }

  Widget updateTempWidget(String city) {
    return FutureBuilder(
      future: getWeather(util.appId, city == null ? util.defaultCity : city),
      builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
        if (snapshot.hasData) {
          Map content = snapshot.data;
          return Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ListTile(
                  title: Text(
                    ' ${content['main']['temp'].toStringAsFixed(0)}°C',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 49.9,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal
                    ),
                  ),
                  subtitle: ListTile(
                    title: Text(
                      'Humidity: ${content['main']['humidity'].toStringAsFixed(0)}%\n'
                      'Min: ${content['main']['temp_min'].toStringAsFixed(0)}°C\n'
                      'Max: ${content['main']['temp_max'].toStringAsFixed(0)}°C',
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        } else {
          return Container();
        }
      }
    );
  }
}

class ChangeCity extends StatelessWidget {

  final _cityFieldController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(
          'Change City'
        ),
        centerTitle: true,
      ),
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("images/white_snow.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                ListTile(
                  title: TextField(
                    decoration: InputDecoration(
                      hintText: 'Enter city',
                    ),
                    controller: _cityFieldController,
                    keyboardType: TextInputType.text,
                  ),
                ),
                ListTile(
                  title: FlatButton(
                    onPressed: () {
                      Navigator.pop(context, {
                        'enter' : _cityFieldController.text
                      });
                    },
                    child: Text(
                      'Get Weather',
                    ),
                    color: Colors.redAccent,
                    textColor: Colors.white70,
                  ),
                )
              ]
            )
          ],
        ),
      ),
    );
  }
}

TextStyle cityStyle() {
  return TextStyle(
    color: Colors.white,
    fontSize: 22.9,
    fontStyle: FontStyle.italic,
  );
}

TextStyle tempStyle() {
  return TextStyle(
    color: Colors.white,
    fontSize: 49.9,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w500
  );
}